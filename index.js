const config = require('./config');
let express = require('express');
let app     = express();
let bb = require('express-busboy');
var path = require("path");

//Directory route handlers
let marketplaceHandler  = require('./EbrizaMkp/mkp.js');
let callDeliveryHandler = require('./callDelivery/delivery.js');
let managementHandler   = require('./management/admin.js');

//DB configuration
let monk = require('monk');
let mongoConnectionString = config.databaseConfig.username + ":" + config.databaseConfig.password +
                            "@" +config.databaseConfig.url + ":" +config.databaseConfig.port + "/" + config.databaseConfig.database + "?authSource=admin";
const db = monk(mongoConnectionString);

db.catch(function(err) {
    console.log("mongoConnectionString:", mongoConnectionString);
    console.log(err)
});

//Set-up settings
app.set('view engine', 'pug');
app.set("views", path.join(__dirname, "views"));

// parse requests of content-type - application/json
bb.extend(app);

//MiddleWare to catch all request made to the NodeJS server
app.use(function(req, res, next) {

    console.info("\nReceived Request:\n================================");
    console.info("Requested Resource:", req.method, req.url);
    console.info("Headers:", req.headers);
    console.info("Body:", req.body);
    next();
});

//Add the middleware to pass the DB object to all the requests
app.use(function(req,res,next){
    req.db = db;
    next();
});

//Define Ebriza Marketplace Handler
app.use("/marketplace", marketplaceHandler);
app.use("/feed", callDeliveryHandler);
app.use("/management", managementHandler);

app.get('/',function(req,res) {
    res.send('Ebriza Call Delivery App!');
});

// catch 404
app.use(function(req, res, next) {
    res.send('Not found!');
});

app.listen(config.app.port,function() {});