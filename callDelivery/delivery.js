var express = require('express');
var EbrizaClient = require('../lib/ebrizaClient');
var config = require('../config');
var constants = require('../constants');
var router = express.Router();
var xmlify = require('xmlify');




function _checkAllProductsAreCancelled(products) {

    if(!products) {
        return false;
    }

    for (let product of products) {
        if (product && product.canceled == false) {
            return false;
        }
    }

    return true;
}


function _computePaymentsMethods(payments, normalizedOrder, order) {

    var orderTotal = 0;

    normalizedOrder['Pay_Cash'] = 0;
    normalizedOrder['Pay_Card'] = 0;
    normalizedOrder['Pay_OP']   = 0;
    normalizedOrder['OrderTotal']=0;

    //double check to avoid problems
    if(!payments) {
        return;
    }

    payments.forEach((item) => {

        let totalValue = item.value*item.units;
        orderTotal += totalValue;
        switch(item.type) {
            case constants.paymentTypes.CASH_ON_DELIVERY:
            case constants.paymentTypes.CASH:
                normalizedOrder['Pay_Cash'] = totalValue;
                break;
            case constants.paymentTypes.CREDIT_CARD:
                normalizedOrder['Pay_Card'] = totalValue;
                break;
            case constants.paymentTypes.BANK_TRANSFER:
                normalizedOrder['Pay_OP'] = totalValue;
                break;
        }
    });

    if(order && order.total) {
        normalizedOrder['OrderTotal'] = order.total;
    } else {
        normalizedOrder['OrderTotal'] = orderTotal;
    }
}


function _normalizeBill(order, client) {


    var dateAndTime = order.timestamp.split('T');

    var normalizedOrder = {
        OrderIDASiS    : order.orderNo?order.orderNo:'-',
        OrderDate      : dateAndTime[0], //retrieve only date
        OrderHour      : dateAndTime[1], //retrieve only hour
        OrderState     : "DESCHISA",
        ClientName     : (client.firstName ? client.firstName : "-") + " " + (client.lastName ? client.lastName : " "),
        Phone          : client.phone ? client.phone : "-",
        Email          : client.email ? client.email : "-",
        OrderSource    : order.OrderProvider ? order.OrderProvider : 'Ebriza',
        OrderNumHip    : order.externalOrderNo?order.externalOrderNo:"-",
        OrderNumRestHip: "-" ,
        OrderDeliveryHour: order.deliveryDate.split('T')[1]

    };

    _computePaymentsMethods(order.payments, normalizedOrder);

    //Client Details
    client.addresses.forEach((address) => {
        if(address.isDeliveryAddress) {
            normalizedOrder['ShippingAddress'] = address.street + ", " + address.city + ", " + address.country;
        }
    });

    return normalizedOrder;
}

function _updateBillToOrder(order) {

    var updates = {};

    _computePaymentsMethods(order.paymentItems, updates, order);

    switch(order.status) {
        case constants.orderStates.COMPLETED:
            updates['OrderState'] = 'Realizat';
            break;
        case constants.orderStates.CANCELED:
            console.log("[ORDER] Cancelled Order received");
            updates['OrderState'] = 'Anulat';
            break;
        default:
            updates['OrderState'] = 'Realizat';
    }

    let allCancelled = _checkAllProductsAreCancelled(order.products);
    if (allCancelled) {
        updates['OrderState'] = 'Anulat';
    }

    updates['OrderIDASiS'] = order.orderNo ? order.orderNo : '-';

    console.log("Updating Bill to Final Order:", updates);

    return {
        '$set' : updates
    }
}

// Home page route.
router.get('/:clientId/xml', function (req, res) {

    let clientId = req.params.clientId;

    var db = req.db;
    let ordersCollection = db.get('orders');

    var now = new Date();

    var dateForOrders = now.toISOString().split('T')[0];

    db.get('clients').find({clientId: clientId}).then((results) => {

        if(!results) {
            res.send("This client was not subscribed to the application");
            return;
        }

        var ebrizaClient = new EbrizaClient(config.ebrizaCredentials.publicKey, config.ebrizaCredentials.clientSecret);
        ordersCollection.find({clientId: clientId,  OrderDate: dateForOrders}, {clientId:0, ebrizaOrderId:0, _id:0}).then((results) => {

            var returnXml = xmlify(results, 'Orders');
            res.type('application/xml');
            console.log(returnXml);
            res.send(returnXml);
        });
    });
});


router.post('/bills/:clientId', (req, res) => {
    var ebrizaClient = new EbrizaClient(config.ebrizaCredentials.publicKey, config.ebrizaCredentials.clientSecret);
    let clientId = req.params.clientId;
    let orderId = req.body.ID;
    let entityType = req.body.Entity;
    let db = req.db;
    let ordersCollection = db.get('orders');

    //Order is only generated when everything finished. So, we must set some data to the finished object.
    if (entityType == constants.webhooksEntityTypes.ORDER) {
        ebrizaClient.getOrderById(clientId, orderId, (result) => {

            //We cannot upsert order to bill because we do not have all the necessary fields
            let dbUpdate = _updateBillToOrder(result);
            ordersCollection.update({
                ebrizaOrderId: orderId
            }, dbUpdate).then((updatedOrder) => {
                console.log("Updated bill to order in the database.");
                res.send('Success');
            });
        }, (error) => { res.send("Eroare:" + error);});
    }

    //If the changed entity is bill.
    if (entityType == constants.webhooksEntityTypes.BILL) {
        ebrizaClient.getBillById(clientId, orderId,
            (order) => {
                console.log("Received Bill Changes from Ebriza:", order);

                if (!order.isDelivery) {
                    console.log("This bill is not of type delivery");
                    res.send("Success");
                    return;
                }

                //Add the clientId to the entry;
                order['ebrizaClientId'] = clientId;

                //Retrieve client details
                ebrizaClient.getClientById(clientId, order.clientID, (client) => {

                        var orderDataToStore = _normalizeBill(order, client);
                        orderDataToStore['ebrizaOrderId'] = orderId;
                        orderDataToStore['clientId'] = clientId;


                        ordersCollection.update({
                            ebrizaOrderId: orderId
                        }, orderDataToStore, {upsert: true}).then((upsertedOrder) => {
                            console.log("Stored order to database");
                            res.send('Success');
                        });

                    },
                    (error) => {
                        res.send("Eroare:" + error);
                    });
            }, (error) => {
                res.send("Eroare:" + error);
            });
    }
});

module.exports = router;