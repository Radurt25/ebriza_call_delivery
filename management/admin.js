var express = require('express');
var EbrizaClient = require('../lib/ebrizaClient');
var config = require('../config');
var router = express.Router();
var bcrypt = require('bcrypt');
var cookieParser = require('cookie-parser');
var session = require('express-session');


router.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
router.use(session({
    key: 'user_sid',
    secret: '123ewq!@#EWQ',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/management/dashboard');
    } else {
        next();
    }
};

router.get('/', sessionChecker, (req, res) => {
    res.render('login');
});



// route for user Login
router.route('/login')
    .get(sessionChecker, (req, res) => {
        res.render('login');
    })
    .post((req, res) => {
        var username = req.body.username,
            password = req.body.password;

        var usersCollection = req.db.get("users");

        usersCollection.findOne({username: username}).then(function (user) {

            if (!user) {
                res.redirect('/management/login');
                return;
            }

            if (!bcrypt.compareSync(password, user.password)){
                res.redirect('/management/login');
            } else {
                req.session.user = user.username;
                res.redirect('/management/dashboard');
            }
        });
});


router.get('/dashboard', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {

        req.db.get('clients').find({}).then((clients) => {
            res.render('dashboard', {clients: clients});
        });

    } else {
        res.redirect('/management/login');
    }
});

// GET /logout
// route for user logout
router.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/management/');
    } else {
        res.redirect('/management/login');
    }
});


module.exports = router;