var express = require('express');
const config = require('../config');
var EbrizaClient = require('../lib/ebrizaClient');
var router = express.Router();
var constants = require('../constants');

// Home page route.
router.post('/', function (req, res) {

    //Check the client Id header
    if (!req.headers['ebriza-clientid']) {
        res.send('No ebriza clientId was found in the headers!');
        return;
    }

    //Check that ActionType Exists
    let bodyPayload = req.body;

    if (bodyPayload.ActionType == null) {
        res.send('No ActionType was defined in the payload!');
        return;
    }

    let db = req.db;
    let ebrizaClientId = req.headers['ebriza-clientid'];
    let clientsCollection = db.get('clients');

    if (bodyPayload.ActionType === constants.ebrizaMarketplaceActions.UNSUBSCRIBE) {
        //If ActionType is unsubscribe, delete the record from DB
        clientsCollection.remove({clientId: ebrizaClientId, locationId: bodyPayload.LocationID }).then((deletedItem) => {

            console.log("Client Removed from the database. Unsubscribing from orders.");
            var ebrizaClient = new EbrizaClient(config.ebrizaCredentials.publicKey, config.ebrizaCredentials.clientSecret);

            ebrizaClient.unsubscribeToBills(ebrizaClientId, bodyPayload.locationId, (result) => {
                    res.send('The Client was succesfully unsubscribed!');
                    console.log("UnSubscribed to Orders", result);
                },
                (error) => {
                    res.send("Eroare:" + error);
                }
            );
        });

    } else {

        var ebrizaClient = new EbrizaClient(config.ebrizaCredentials.publicKey, config.ebrizaCredentials.clientSecret);

        console.log("[MKP Subscribe]1. Getting location details...");
        //First get Location Name.
        ebrizaClient.getLocationDetails(ebrizaClientId, (locationDetails) => {

            //Get the name of the location for this clientId and location as in the Subscribe Ebent
            var locationName = 'N/A';
            locationDetails.forEach((item) => {
                if(item.id === bodyPayload.LocationID) {
                    locationName = item.name;
                }
            });

            /** We get here only for SUBSCRIBE and UPDATE <=> UPSERT **/
            var client = {
                clientId: ebrizaClientId,
                settings: bodyPayload.Configuration,
                locationId: bodyPayload.LocationID,
                locationName: locationName //For now, take only the first one.
            };

            console.log("[MKP Subscribe]2. Started upserting Client to Database:", client);
            //Every App is installed on a location level. So Upsert settings to DB for client
            clientsCollection.update({
                clientId: ebrizaClientId,
                locationId: bodyPayload.LocationID
            }, client, {upsert: true}).then((upsertedClient) => {

                console.log("[MKP Subscribe]3. Finished upserting client to db", upsertedClient);
                ebrizaClient.subscribeToBills(ebrizaClientId, bodyPayload.locationId,
                    (result) => {

                        console.log("[MKP Subscribe]4. Subscribed to Bills", result);
                        ebrizaClient.subscribeToOrders(ebrizaClientId, bodyPayload.locationId, (result) => {
                            console.log("[MKP Subscribe]5. Subscribed to Orders", result);
                            res.send("Success");
                        },(error)=> { res.send("Eroare:" + error);} )
                    },
                    (error) => {
                        res.send("Eroare:" + error);
                    }
                );
            });

        }, (error)=> { res.send("Eroare:" + error);});
    }
});

module.exports = router;