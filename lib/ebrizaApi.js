﻿var btoa = require( "btoa" );
var request = require("request");
const config = require('../config');
/**
* ebrizaApi client library.
*
* This library will handle the authentication, token generation and refresh to communicate with Ebriza API.
* All dates are and must remane in UTC format!!
*/
module.exports = function ebrizaApi(appId, secretKey, clientId) {

    if (!appId) {
        console.error('Missing "appId" parameter!');
        return null;
    }

    if (!secretKey) {
        console.error('Missing "secretKey" parameter!');
        return null;
    }

    if (!clientId) {
        console.error('Missing "clientId" parameter!');
        return null;
    }

    var api = {
        appId: appId,
        clientId: clientId,
        serverUrl: config.app.ebrizaUrl
    };

    var secretKey = secretKey;
    var tokenContextCache = null;

    api.post = post;
    api.postRaw = postRaw;
    api.get = get;
    api.getRaw = getRaw;

    return api;

    //Public methods
    //

    /**
    * post function.
    *
    * @param {String} url - The endoint relative url you want to call (ex: bills/open).
    * @param {Object} data - The object you want to post. Will be serialized in JSON.
    * @param {Function} success - Callback for success. Returns the deserialized JSON object. function success(object) {}
    * @param {Function} error - Callback for error. function error(httpStatusCode, errorMessage) {}
    */
    function post(url, data, success, error) {
        if (!url) {
            console.error('Missing "url" parameter!');
            return false;
        }

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        processRequest('POST', url, data, function (response) {
            success(response);
        }, error);
    }

    /**
    * postRaw function.
    *
    * @param {String} url - The endoint relative url you want to call (ex: bills/open).
    * @param {Object} data - The object you want to post. Will be serialized in JSON.
    * @param {Function} success - Callback for success. Returns the string response. function success(stringResponse) {}
    * @param {Function} error - Callback for error. function error(httpStatusCode, errorMessage) {}
    */
    function postRaw(url, data, success, error) {
        if (!url) {
            console.error('Missing "url" parameter!');
            return false;
        }

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        processRequest('POST', url, JSON.stringify(data), success, error);
    }

    /**
    * get function.
    *
    * @param {String} url - The endoint relative url you want to call (ex: bills/open).
    * @param {Object} params - The object you want to send as query string parameter. Will be parsed, encoded and attached to the url.
    * @param {Function} success - Callback for success. Returns the deserialized JSON object. function success(object) {}
    * @param {Function} error - Callback for error. function error(httpStatusCode, errorMessage) {}
    */
    function get(url, params, success, error) {
        if (!url) {
            console.error('Missing "url" parameter!');
            return false;
        }

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        processRequest('GET', url, params, function (response) {
            success(JSON.parse(response));
        }, error);
    }

    /**
    * getRaw function.
    *
    * @param {String} url - The endoint relative url you want to call (ex: bills/open).
    * @param {Object} params - The object you want to send as query string parameter. Will be parsed, encoded and attached to the url.
    * @param {Function} success - Callback for success. Returns the string response. function success(stringResponse) {}
    * @param {Function} error - Callback for error. function error(httpStatusCode, errorMessage) {}
    */
    function getRaw(url, params, success, error) {
        if (!url) {
            console.error('Missing "url" parameter!');
            return false;
        }

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        processRequest('GET', url, params, success, error);
    }

    //Private methods
    //

    function processRequest(method, resource, data, success, error) {

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        try {
            var token = getToken(function (tokenContext) {

                var url = getUrl(resource);
                var auth = tokenContext.tokenType + ' ' + tokenContext.token;

                function unauthorizedCheck(status, message) {
                    if (status == 401) { //Unauthorized
                        tokenContextCache.token = null;
                        processRequest(method, resource, data);
                    }
                    else
                        error(status + ':' + message);
                };

                if (method == 'GET') 
                    getRequest(url, auth, data, success, unauthorizedCheck);
                else
                    postRequest(url, JSON.stringify(data), auth, success, unauthorizedCheck);
            });
        }
        catch (err) {
            error(err);
        }
    }

    function getToken(callback) {
        if (tokenContextCache && tokenContextCache.tokenExpiration && tokenContextCache.tokenExpiration.getTime() > new Date().getTime() && tokenContextCache.token)
            return tokenContext;

        var encodedSecretKey = btoa(api.appId + ":" + secretKey);

        postRequest(getUrl('token'), "grant_type=client_credentials", "Basic " + encodedSecretKey , function (data) {
            var tokenData = JSON.parse(data);
            tokenContextCache = {
                token: tokenData.access_token,
                tokenType: tokenData.token_type,
                tokenExpiration: new Date().getTime() + (tokenData.expires_in * 1000)
            };
            callback(tokenContextCache);
        }, function (status, message) {
            throw 'Error: [' + status + '] ' + message;
        });
    }

    //Communication
    //

    function getRequest(url, auth, params, success, error) {

        //Ensure success and error functions exists
        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };

        if (params) {
            url += '?' + encodeParams(params);
        }

        if (url.endsWith('/get')) {
            url = url.substring(0, url.length - '/get'.length);
        }

        var headers =  {
            "ebriza-clientid" : api.clientId
        };

        if (auth) {
            headers["Authorization"] = auth;
        }

        console.log("\n--------------------");
        console.log("GET Request to:", url);
        console.log("GET Headers for request:", headers);
        console.log("\n");

        return request({
            headers: headers,
            uri: url,
            method: 'GET'
        }, function (err, res, body) {

            if (err || res.statusCode != 200) {
                error(res.statusCode, body);
            } else {
                success(body);
            }
        });
    }

    function postRequest(url, data, auth, success, error) {

        if (typeof success != 'function') success = function () { };
        if (typeof error != 'function') error = function () { };


        var headers =  {
            "ebriza-clientid" : api.clientId,
            "Content-Type"    : "application/json"
        };

        if (auth) {
            headers["Authorization"] = auth;
        }

        console.log("\n--------------------");
        console.log("POST Request to:", url);
        console.log("POST Headers for request:", headers);
        console.log("POST Body:", data);
        console.log("\n");

        return request({
            headers: headers,
            uri: url,
            body: data,
            method: 'POST'
        }, function (err, res, body) {

            if (err) {
                error(res.status, body);
            } else {
                success(body);
            }
        });
    }

    function encodeParams(object) {
        var encodedString = '';
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if (encodedString.length > 0) {
                    encodedString += '&';
                }
                encodedString += encodeURI(prop + '=' + object[prop]);
            }
        }
        return encodedString;
    }

    function getUrl(resource) {

        if (resource.startsWith(api.serverUrl + '/api'))
            resource = resource.substring((api.serverUrl + '/api').length);
        if (resource.startsWith(api.serverUrl))
            resource = resource.substring((api.serverUrl).length);

        if (api.serverUrl[0] == '/')
            api.serverUrl = api.serverUrl.substring(1);
        if (api.serverUrl[api.serverUrl.length - 1] == '/')
            api.serverUrl = api.serverUrl.substring(0, api.serverUrl.length);
        if (resource[0] == '/')
            resource = resource.substring(1);
        if (resource[resource.length - 1] == '/')
            resource = resource.substring(0, resource.length);

        return api.serverUrl + '/api/' + resource;
    }
};