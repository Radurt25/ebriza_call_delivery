var ebrizaApi = require('../lib/ebrizaApi.js');
var config = require('../config');

module.exports = class EbrizaClient {

    constructor(publicKey, clientSecret) {
        this.publicKey = publicKey;
        this.clientSecret = clientSecret;
    }

    retrieveOrderList(clientId, locationId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        return api.get('orders', {locationid: locationId}, successCallback, errorCallback);
    }

    getLocationDetails(clientId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        return api.get('company/locations', {}, successCallback, errorCallback);
    }

    getOrderById(clientId, orderId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        return api.get('orders/get/'+orderId, orderId, successCallback, errorCallback);
    }

    getBillById(clientId, orderId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        return api.get('bills/get/'+orderId, orderId, successCallback, errorCallback);

    }

    getClientById(ebrizaClientId, userClientId, successCallback, errorCallback) {
        var api = new ebrizaApi(this.publicKey, this.clientSecret, ebrizaClientId);
        return api.get('clients/get/'+userClientId, userClientId, successCallback, errorCallback);
    }

    subscribeToBills(clientId, locationId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        var ordersSubscriptionUrl = config.app.baseUrl + "/feed/bills/" + clientId;

        var data = {
            'callbackUrl': ordersSubscriptionUrl,
            'entity': 1,
            'locationID': locationId
        };
        return api.post('webhooks/subscribe', data, successCallback, errorCallback);
    }

    subscribeToOrders(clientId, locationId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);
        var ordersSubscriptionUrl = config.app.baseUrl + "/feed/bills/" + clientId;

        var data = {
            'callbackUrl': ordersSubscriptionUrl,
            'entity':0,
            'locationID': locationId
        };
        return api.post('webhooks/subscribe', data, successCallback, errorCallback);
    }

    unsubscribeToBills(clientId, locationId, successCallback, errorCallback) {

        var api = new ebrizaApi(this.publicKey, this.clientSecret, clientId);

        var data = {
            'entity': 1,
            'locationID': locationId
        };
        return api.post('webhooks/unsubscribe', data, successCallback, errorCallback);
    }
};