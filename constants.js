const constants = {

    ebrizaMarketplaceActions : {
            SUBSCRIBE  : 0,
            UNSUBSCRIBE: 1,
            UPDATE     : 2
    },
    webhooksEntityTypes: {
        ORDER: 0,
        BILL:  1
    },
    paymentTypes : {
        CASH_ON_DELIVERY: 0,
        CASH: 1,
        CREDIT_CARD: 2,
        BANK_TRANSFER: 3,
        PROTOCOL: 4,
        CASH_WITH_INVOICE_RECEIPT: 5,
        MEAL_TICKET : 6,
        FISCAL_PROTOCOL: 7,
        CASH_NOT_FISCAL: 8
    },
    orderStates : {
        PENDING : 0,
        SHIPPED:  1,
        CONFIRM_PAYMENT: 2,
        COMPLETED: 3,
        CANCELED: 4
    }
};

module.exports = constants;